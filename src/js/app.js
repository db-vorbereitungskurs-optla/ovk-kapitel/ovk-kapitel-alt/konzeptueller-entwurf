import './common';
import './formeln';
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'
import 'bootstrap';
//import './h5p-resizer';

import hljs from 'highlight.js/lib/core';
import python from 'highlight.js/lib/languages/python';
import sql from 'highlight.js/lib/languages/sql';



hljs.registerLanguage('python', python);
hljs.registerLanguage('sql', sql);
hljs.initHighlightingOnLoad();

if (module.hot) {
    module.hot.accept();
}
// my comment
(function() {
    if (document.body.querySelector('math') ||
        document.body.textContent.match(/(?:\$|\\\(|\\\[|\\begin\{.*?})/)) {
        if (!window.MathJax) {
            window.MathJax = {
                tex: {
                    inlineMath: {
                        '[+]': [
                            ['$', '$']
                        ]
                    }
                }
            };
        }
        var script = document.createElement('script');
        script.src = 'https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js';
        document.head.appendChild(script);
    }
})();